import React from 'react';
import clsx from 'clsx';
import { withTheme, withStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import CssBaseline from '@material-ui/core/CssBaseline';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import { Link } from 'react-router-dom';

import styles from './styles/Menu';

class Menu extends React.PureComponent {
  constructor(props){
    super(props);

    this.state = {
      open: false,
    }
  }
  
  menu = JSON.parse(localStorage.getItem('menu')) || [];

  handleDrawerOpen = () => {
    this.setState({ open : true });
  }

  handleDrawerClose = () => {
    this.setState({ open : false });
  };

  render(){
    const { classes, theme } = this.props;

    return (
      <div className={classes.root}>
        <CssBaseline />
        <AppBar
          position="fixed"
          className={clsx(classes.appBar, {
            [classes.appBarShift]: this.props.open,
          })}
        >
          <Toolbar>
            <IconButton
              color="inherit"
              aria-label="open drawer"
              onClick={this.handleDrawerOpen}
              edge="start"
              className={clsx(classes.menuButton, this.state.open && classes.hide)}
            >
              <MenuIcon />
            </IconButton>
            <Typography variant="h6" noWrap>
              Distribuidora de Produtos Alimentícios Trevo LTDA
            </Typography>
          </Toolbar>
        </AppBar>
        <Drawer
          className={classes.drawer}
          variant="persistent"
          anchor="left"
          open={this.state.open}
          classes={{
            paper: classes.drawerPaper,
          }}
        >
          <div className={classes.drawerHeader}>
            <IconButton onClick={this.handleDrawerClose}>
              {theme.direction === 'ltr' ? <ChevronLeftIcon /> : <ChevronRightIcon />}
            </IconButton>
          </div>
          {this.menu.map(grupo =>
            <div key={`container_grupo_${grupo.grupo}`}>
              <Divider />
              <List>
                {grupo.items.map((element, idx) =>
                  <ListItem button key={`${element.rota}_${idx}`}>
                    <Link to={`/${element.rota}`} style={{ textDecoration: 'none' }} >
                      <ListItemText primary={element.descricao} />
                    </Link>
                  </ListItem>
                )}
              </List>
            </div>
          )}
          <Divider />
          <List>
            {['Sair'].map((text, index) => (
              <ListItem button key={text}>
                <Link to={`/${text.toLowerCase()}`}  style={{ textDecoration: 'none' }} >
                  <ListItemText primary={text} />
                </Link>
              </ListItem>
            ))}
          </List>
        </Drawer>
        <main
          className={clsx(classes.content, {
            [classes.contentShift]: this.props.open,
          })}
        >
          <div className={classes.drawerHeader} />
          {this.props.children}
        </main>
      </div>
    );
  }

}

export default withStyles(styles)(withTheme(Menu));
