import React from 'react';
import loginService from '../../services/loginService';

export default function Logout({ history }) {

  loginService.logout();
  history.push('/');

	return (
    <>
    </>
	);
}
