import React, { PureComponent } from 'react';
import { withRouter } from 'react-router-dom';
import NumberFormat from 'react-number-format';
import Container from '@material-ui/core/Container';

import api from '../../services/api';
import loginService from '../../services/loginService';
import withMessageHandler from '../../hoc/withMessageHandler/withMessageHandler';

import ProdutosForm from './Form';
import Produto from '../../models/Produto';
import ActionTable from '../../components/ActionTable';
import ConfirmDialog from '../../components/ConfirmDialog';

import camera from '../../assets/camera.svg';

const url = '/produtos';

class Produtos extends PureComponent {
  constructor(props){
    super(props);

    if(!loginService.verificaLogin()){
      props.history.push('/');
    }

    this.state = {
      currentObj: new Produto(),
      data: [],
      open: false,
      openConfirm: false,
      columns: [
        {
          title: 'ID',
          field: 'id',
          render: rowData => rowData.getId()
        },
        {
          title: 'Nome',
          field: 'descricao',
          render: rowData => rowData.getDescricao()
        },
        {
          title: 'Preço',
          field: 'preco',
          render: rowData => <NumberFormat value={rowData.getPreco()} displayType='text' thousandSeparator="." decimalSeparator="," isNumericString prefix="R$"/>
        },
        {
          title: 'Quantidade',
          field: 'quantidade',
          render: rowData => rowData.getQuantidade()
        },
        {
          title: 'Imagem',
          field: 'thumbnail',
          render: rowData => {
            const
              thumbnail = rowData.getThumbnail(),
              thumbnailUrl = `${loginService.apiUrl}/files/${thumbnail}`,
              url = (!!thumbnail) ? thumbnailUrl : camera;

            return <img src={url} style={{width: 50}} alt={`Foto do produto ${rowData.getDescricao()}`}/>
          }
        }
      ]
    }
  }

  componentDidMount(){
    api.request({
      url,
      method: 'get'
    })
    .then(response => this.setState({ data: Produto.fromDataRequest(response.data.items) }))
    .catch(this.props.handleError);
  }

  setOpen = (value) => this.setState({ open : value });
  setOpenConfirm = (value) => this.setState({ openConfirm : value });
  setData = (value) => this.setState({ data : value });
  setCurrentObj = (value) => this.setState({ currentObj : value });

  handleClickOpen = (event, rowData) => {
    this.setCurrentObj(rowData);
    this.setOpen(true);
  };

  handleClickOpenAdd = (event, rowData) => {
    this.setCurrentObj(new Produto());
    this.setOpen(true);
  };

  handleClose = (event, produto) => {
    if(event.target.id === 'confirm' || event.target.parentElement.id === 'confirm'){
      const
        dados = [...this.state.data],
        formData = produto.getFormData(),
        header = {
          'Accept': 'application/json',
          'Content-Type': `multipart/form-data`,
        };

      produto.setId(this.state.currentObj.getId());
      this.setCurrentObj(produto);

      if(this.state.currentObj.getId() === null){
        api.request({
          url,
          method: 'post',
          data: formData,
          header
        }).then(response => {
          produto.setId(response.data.id);
          produto.setThumbnail(response.data.thumbnail);
          dados.push(produto);
          this.setData(dados);
          this.props.handleSuccess('Produto salvo com sucesso!');
        }).catch(this.props.handleError);
      } else {
        const index = this.state.data.indexOf(this.state.currentObj);

        api.request({
          url: `${url}/${this.state.currentObj.id}`,
          method: 'put',
          data: formData,
          header
        }).then(response => {
          produto.setThumbnail(response.data.thumbnail);
          dados[index] = produto;
          this.setData(dados);
          this.props.handleSuccess('Produto salvo com sucesso!');
        }).catch(this.props.handleError);
      }
    }

    this.setOpen(false);
  };

  handleDelete = (event, rowData) => {
    this.setCurrentObj(rowData);
    this.setOpenConfirm(true);
  }

  handleCloseConfirm = (event) => {
    if(event.target.id === 'confirm' || event.target.parentElement.id === 'confirm'){
      api.request({
        url: `${url}/${this.state.currentObj.id}`,
        method: 'delete'
      }).then(response => {
        const newData = this.state.data.filter(dado => dado.getId() !== this.state.currentObj.getId());
        this.setData(newData);
        this.props.handleSuccess('Produto excluído com sucesso!');
      }).catch(this.props.handleError);
    }
    this.setOpenConfirm(false);
  }

	render() {
    return (
      <Container fixed>
        <ActionTable
          title={'Produtos'}
          columns={this.state.columns}
          data={this.state.data}
          setData={this.setData}
          onAdd={this.handleClickOpenAdd}
          onEdit={this.handleClickOpen}
          onDelete={this.handleDelete}
        >
        </ActionTable>
        <ProdutosForm
          open={this.state.open}
          handleClose={this.handleClose}
          currentObj={this.state.currentObj}
        />
        <ConfirmDialog
          open={this.state.openConfirm}
          handleClose={this.handleCloseConfirm}
          title={"Tem certeza que deseja excluir o registro?"}
        />
      </Container>
    );
	};
}

export default withRouter(withMessageHandler(Produtos));
