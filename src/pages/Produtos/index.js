import React from 'react';
import Menu from '../Common/Menu';
import Produtos from './Produtos';
import CssBaseline from '@material-ui/core/CssBaseline';

export default function ProdutosIndex(){
  return (
    <>
      <CssBaseline />
      <Menu/>
      <Produtos/>
    </>
	);
}
