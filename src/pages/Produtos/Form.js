import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import NumberFormat from 'react-number-format';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';

import loginService from '../../services/loginService';
import api from '../../services/api';

import FormDialog from '../../components/FormDialog';
import camera from '../../assets/camera.svg';
import Produto from '../../models/Produto';
import styles from './styles/Form';
import './styles/form.css';

function NumberFormatCustom(props) {
  const { inputRef, onChange, ...other } = props;

  return (
    <NumberFormat
      {...other}
      getInputRef={inputRef}
      onValueChange={values => {
        onChange({
          target: {
            value: values.value,
          },
        });
      }}
      thousandSeparator="."
      decimalSeparator=","
      isNumericString
      prefix="R$"
    />
  );
}

class ProdutosForm extends React.PureComponent {

  constructor(props) {
    super(props);
    this.state = {
      preview: null,
      descricao: '',
      preco: '',
      quantidade: '',
      thumbnail: null
    };
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    let thumb = this.state.thumbnail;
    if(!!thumb && thumb !== prevState.thumbnail){
      if(typeof thumb === 'object'){
        thumb = URL.createObjectURL(thumb);
        this.setPreview(thumb);
      }
    }
  };

  setDescricao = (value) => this.setState({ descricao : value });
  setPreco = (value) => this.setState({ preco : value });
  setQuantidade = (value) => this.setState({ quantidade : value });
  setThumbnail = (value) => this.setState({ thumbnail : value });
  setPreview = (value) => this.setState({ preview : value });

  onEnter = () => {
    const
      currentObj = this.props.currentObj,
      thumbnail = currentObj.getThumbnail();

    this.setPreco(currentObj.getPreco() || '');
    this.setDescricao(currentObj.getDescricao() || '');
    this.setQuantidade(currentObj.getQuantidade() || '');
    if(!!thumbnail){
      api.request({
        url: `${loginService.apiUrl}/files/${thumbnail}`,
        method: 'get',
        responseType: 'blob',
      }).then(response => {
        const thumb = response.data;
        if (typeof thumb === 'object'){
          const urlPreview = URL.createObjectURL(thumb);
          this.setThumbnail(thumb);
          this.setPreview(urlPreview);
        }
      }).catch(this.props.handleError);
    }
  }

  onClose = (event) => {
    const produto = Produto.fromData(this.state);
    this.props.handleClose(event, produto);
  }

  render(){
    return (
      <FormDialog open={this.props.open} onEnter={this.onEnter} handleclose={this.onClose} title={"Salvar Produto"}>
        <div className={this.props.classes.root}>
          <Grid container spacing={1}>
            <Grid item xs={12}>
              <label
        				id="thumbnail"
        				style={{ backgroundImage: `url(${this.state.preview})` }}
                className={this.state.thumbnail ? 'has-thumbnail' : ''}
        			>
        				<input type='file' onChange={event => this.setThumbnail(event.target.files[0])}/>
        				<img src={camera} alt='select img'/>
        			</label>
            </Grid>
            <Grid item xs={12}>
              <TextField
                autoFocus
                margin = "dense"
                id = "descricao"
                label = "Nome"
                type = "text"
                variant = "outlined"
                value = {this.state.descricao}
                onChange={event => this.setDescricao(event.target.value)}
                fullWidth
              />
            </Grid>
            <Grid item xs={6}>
              <TextField
                margin = "dense"
                id = "preco"
                label = "Preço"
                type = "text"
                variant = "outlined"
                value = {this.state.preco}
                onChange={event => this.setPreco(event.target.value)}
                InputProps={{
                  inputComponent: NumberFormatCustom,
                }}
                fullWidth
              />
            </Grid>
            <Grid item xs={6}>
              <TextField
                margin = "dense"
                id = "quantidade"
                label = "Quantidade"
                type = "text"
                variant = "outlined"
                value = {this.state.quantidade}
                onChange={event => this.setQuantidade(event.target.value)}
                fullWidth
              />
            </Grid>
          </Grid>
        </div>
      </FormDialog>
    );
  }
}

export default withStyles(styles)(ProdutosForm);
