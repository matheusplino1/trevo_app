import React from 'react';
import Menu from '../Common/Menu';
import Visitas from './Visitas';
import CssBaseline from '@material-ui/core/CssBaseline';

export default function VisitasIndex(){
  return (
    <>
      <CssBaseline />
      <Menu/>
      <Visitas/>
    </>
	);
}
