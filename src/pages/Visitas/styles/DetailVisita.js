const styles = theme => ({
  submit: {
    transform: "translateY(-50%)",
    top: "50%",
    position: "relative",
    marginRight: theme.spacing(2),
    marginLeft: theme.spacing(2),
  },
  conteudoContainer: {
    padding: theme.spacing(2, 2, 1),
  }
});

export default styles;
