const styles = theme => ({
  root: {
    flexGrow: 1,
    overflow: 'hidden'
  },
});

export default styles;
