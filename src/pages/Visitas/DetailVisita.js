import React from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Edit from '@material-ui/icons/Edit';
import Box from '@material-ui/core/Box';

import styles from './styles/DetailVisita';

class DetailVisita extends React.PureComponent {
  handleOpenEdit = event => {
    this.props.setCurrentObjById(this.props.visita.id);
    this.props.handleOpen(event);
  }

  render(props){
    return (
      <>
        <CssBaseline />
        <Grid container spacing={1}>
          <Grid item xs={12} sm={10}>
            <Grid container spacing={1} className={this.props.classes.conteudoContainer}>
              <Grid item xs={6} sm={6}>
                <Typography variant="body2" gutterBottom>
                  <strong>Cliente: </strong>
                  {this.props.getCliente(this.props.visita.cliente)}
                </Typography>
              </Grid>
              <Grid item xs={6} sm={6}>
                <Typography variant="body2" gutterBottom>
                  <strong>Tipo: </strong>
                  {this.props.getVisitaTipo(this.props.visita.visitatipo)}
                </Typography>
              </Grid>
              <Grid item xs={6} sm={6}>
                <Typography variant="body2" gutterBottom>
                  <strong>Status: </strong>
                  {this.props.getVisitaStatus(this.props.visita.visitastatus)}
                </Typography>
              </Grid>
              <Grid item xs={6} sm={6}>
                <Typography variant="body2" gutterBottom>
                  <strong>Data: </strong>
                  {new Date(this.props.visita.data).toLocaleDateString('en-GB')}
                </Typography>
              </Grid>
              <Grid item xs={12} sm={12}>
                <Typography variant="body2" gutterBottom>
                  <strong>Observação: </strong>
                  {this.props.visita.observacao}
                </Typography>
              </Grid>
            </Grid>
          </Grid>
          <Grid item xs={12} sm={2}>
            <Box className={this.props.classes.submit}>
              <Button
                fullWidth
                id="edit_visita"
                startIcon={<Edit />}
                variant="outlined"
                color="primary"
                onClick={this.handleOpenEdit}
              >
                Editar Visita
              </Button>
            </Box>
          </Grid>
        </Grid>
      </>
  	);
  }
}

export default withStyles(styles)(DetailVisita);
