import React from 'react';
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import { withRouter } from 'react-router-dom';
import Button from '@material-ui/core/Button';
import AddBox from '@material-ui/icons/AddBox';
import Divider from '@material-ui/core/Divider';
import Container from '@material-ui/core/Container';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';

import api from '../../services/api';
import loginService from '../../services/loginService';
import withMessageHandler from '../../hoc/withMessageHandler/withMessageHandler';

import VisitasForm from './Form';
import styles from './styles/Visitas';
import DetailVisita from './DetailVisita';

const objetoPadrao = { id: null, cliente : "", visitastatus : "", visitatipo : "", observacao : "", data : Date.now()};
const url = '/visitas';

class Visitas extends React.PureComponent {

  constructor(props){
    super(props);

    if(!loginService.verificaLogin()){
      props.history.push('/');
    }

    this.state = {
      open: false,
      openConfirm: false,
      visitas: [],
    }
  }

  componentDidMount(){
    const arrPromises = [];

    arrPromises.push(api.request({
      url: '/usuarios',
      method: 'get'
    }).then(response => {
      const arrClientes = response.data.items;
      this.setState({ arrClientes });
    }).catch(this.props.handleError));

    arrPromises.push(api.request({
      url: '/visitastatus',
      method: 'get'
    }).then(response => {
      const arrVisitastatus = response.data.items;
      this.setState({ arrVisitastatus });
    }).catch(this.props.handleError));

    arrPromises.push(api.request({
      url: '/visitatipo',
      method: 'get'
    }).then(response => {
      const arrVisitaTipo = response.data.items;
      this.setState({ arrVisitaTipo });
    }).catch(this.props.handleError));

    Promise.all(arrPromises).then(() => {
      this.setCurrentObj({...objetoPadrao});
      api.request({
        url,
        method: 'get'
      }).then(response => {
        const visitas = response.data.items;
        this.setState({ visitas });
      }).catch(this.props.handleError);
    });
  }

  setOpen = (value) => this.setState({ open : value });
  setOpenConfirm = (value) => this.setState({ openConfirm : value });
  setCliente = (value) => this.setState({ cliente : value});
  setVisitastatus = (value) => this.setState({ visitastatus : value});
  setVisitatipo = (value) => this.setState({ visitatipo : value});
  setObservacao = (value) => this.setState({ observacao : value});
  setData = (value) => this.setState({ data : value});
  
  setCurrentObj = (data) => {
  	this.setCliente(data.cliente);
  	this.setVisitastatus(data.visitastatus);
  	this.setVisitatipo(data.visitatipo);
  	this.setObservacao(data.observacao);
  	this.setData(data.data);
  	this.setState({ currentObj : {...data}  });
  }

  setCurrentObjById = (id) => {
    const obj = this.state.visitas.filter(element => element.id === id);
    this.setCurrentObj(...obj);
  }

  getObjFromState = () => {
    const state = this.state;
    let obj;

    if(this.state.currentObj.id === null){
      obj = {...objetoPadrao};
    } else {
      obj = {...this.state.currentObj};
    }

    obj.cliente = state.cliente;
    obj.visitastatus = state.visitastatus;
    obj.visitatipo = state.visitatipo;
    obj.observacao = state.observacao;
    obj.data = state.data;

    return obj;
  }

  getVisitaStatus = (id) => this.state.arrVisitastatus.filter(model => model.id === id)[0].descricao;
  getVisitaTipo = (id) => this.state.arrVisitaTipo.filter(model => model.id === id)[0].descricao;
  getCliente = (id) => this.state.arrClientes.filter(model => model.id === id)[0].nome;

  handleOpen = event => {
    if(event.target.id === "add_visita" || event.target.parentElement.id === "add_visita"){
      this.setCurrentObj({...objetoPadrao});
    }
    this.setOpen(true);
  }

  handleClose = (event) => {
    if(event.target.id === 'confirm' || event.target.parentElement.id === 'confirm'){
      const
        currentId = this.state.currentObj.id,
        visita = this.getObjFromState();

      api.request({
        url: `${url}${!!currentId ? `/${currentId}` : ''}`,
        method: `${!!currentId ? `put` : 'post'}`,
        data: visita
      }).then(response => {
        const visitas = [...this.state.visitas];
        if(!!currentId){
          let index;
          visitas.forEach((model, idx) => { if (currentId === model.id) index = idx });
          visitas[index] = visita;
        } else {
          visitas.push(visita);
        }
        this.setState({ visitas });
        this.setCurrentObj(visita);
        this.props.handleSuccess('Visita salva com sucesso!');
      }).catch(this.props.handleError);
    }

    this.setOpen(false);
  };

  render(){
    const classes = this.props.classes;

    return (
      <Container fixed key={'visitas'}>
        <div className={classes.root} key={'visitas_div'}>
          <Grid container spacing={0} key={'visitas_container'}>
            <Grid item xs={12} key={'cabecalho'}>
              <div className={classes.cabecalho}>
                <Grid container spacing={1}>
                  <Grid item xs={12} sm={10} key={'cabecalho_label'}>
                    <Typography variant="h5" gutterBottom>
                      <center>Visitas</center>
                    </Typography>
                  </Grid>
                  <Grid item xs={12} sm={2} key={'cabecalho_button'}>
                    <Box className={classes.submit}>
                      <Button
                        fullWidth
                        id="add_visita"
                        startIcon={<AddBox />}
                        variant="outlined"
                        color="primary"
                        onClick={this.handleOpen}
                      >
                        Nova Visita
                      </Button>
                    </Box>
                  </Grid>
                </Grid>
              </div>
            </Grid>
            {this.state.visitas.map(visita => {
              return (
                <Grid item xs={12} key={`visita_${visita.id}`}>
                  <Divider variant="middle" key={`divider_${visita.id}`} className={classes.divider}/>
                  <div className={classes.item}>
                    <DetailVisita
                      key={visita.id}
                      visita={visita}
                      setCurrentObjById={this.setCurrentObjById}
                      handleOpen={this.handleOpen}
                      getVisitaStatus={this.getVisitaStatus}
                      getVisitaTipo={this.getVisitaTipo}
                      getCliente={this.getCliente}
                    />
                  </div>
                </Grid>
              );
            })}
          </Grid>
          <VisitasForm
            open={this.state.open}
            handleClose={this.handleClose}
          	cliente={this.state.cliente}
          	visitastatus={this.state.visitastatus}
          	visitatipo={this.state.visitatipo}
          	observacao={this.state.observacao}
          	data={this.state.data}
          	setCliente={this.setCliente}
          	setVisitastatus={this.setVisitastatus}
          	setVisitatipo={this.setVisitatipo}
          	setObservacao={this.setObservacao}
          	setData={this.setData}
            arrClientes={this.state.arrClientes}
            arrVisitaTipo={this.state.arrVisitaTipo}
            arrVisitaStatus={this.state.arrVisitastatus}
          />
        </div>
      </Container>
  	);
  }
}

export default withStyles(styles)(withRouter(withMessageHandler(Visitas)));
