import 'date-fns';
import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Grid from '@material-ui/core/Grid';
import DateFnsUtils from '@date-io/date-fns';
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker
} from '@material-ui/pickers';

import FormDialog from '../../components/FormDialog';
import styles from './styles/Form';

class VisitasForm extends React.PureComponent {
  render(){
    return (
      <FormDialog open={this.props.open} handleclose={this.props.handleClose} title={"Salvar Visita"}>
        <div className={this.props.classes.root}>
          <Grid container spacing={1}>
            <Grid item xs={12} sm={6}>
              <FormControl variant="outlined" fullWidth>
                <InputLabel id="select-perfil-label">
                  Cliente
                </InputLabel>
                <Select
                  labelId="select-cliente-label"
                  id="select-cliente"
                  value={this.props.cliente}
                  onChange={event => this.props.setCliente(event.target.value)}
                >
                  {this.props.arrClientes && this.props.arrClientes.map(cliente => (
                    <MenuItem key={cliente.id} value={cliente.id}>{cliente.nome}</MenuItem>
                  ))}
                </Select>
              </FormControl>
            </Grid>
            <Grid item xs={12} sm={6}>
              <FormControl variant="outlined" fullWidth>
                <InputLabel id="select-perfil-label">
                  Tipo de Visita
                </InputLabel>
                <Select
                  labelId="select-cliente-label"
                  id="select-cliente"
                  value={this.props.visitatipo}
                  onChange={event => this.props.setVisitatipo(event.target.value)}
                >
                  {this.props.arrVisitaTipo && this.props.arrVisitaTipo.map(visitatipo => (
                    <MenuItem key={visitatipo.id} value={visitatipo.id}>{visitatipo.descricao}</MenuItem>
                  ))}
                </Select>
              </FormControl>
            </Grid>
            <Grid item xs={12} sm={6}>
              <FormControl variant="outlined" fullWidth>
                <InputLabel id="select-perfil-label">
                  Status da Visita
                </InputLabel>
                <Select
                  labelId="select-cliente-label"
                  id="select-cliente"
                  value={this.props.visitastatus}
                  onChange={event => this.props.setVisitastatus(event.target.value)}
                >
                  {this.props.arrVisitaStatus && this.props.arrVisitaStatus.map(visitastatus => (
                    <MenuItem key={visitastatus.id} value={visitastatus.id}>{visitastatus.descricao}</MenuItem>
                  ))}
                </Select>
              </FormControl>
            </Grid>
            <Grid item xs={12} sm={6}>
              <MuiPickersUtilsProvider utils={DateFnsUtils}>
                <KeyboardDatePicker
                  disableToolbar
                  variant="inline"
                  format="dd/MM/yyyy"
                  margin="normal"
                  id="date-picker-inline"
                  label="Data da Visita"
                  value={this.props.data}
                  onChange={this.props.setData}
                  KeyboardButtonProps={{
                    'aria-label': 'change date',
                  }}
                />
              </MuiPickersUtilsProvider>
            </Grid>
            <Grid item xs={12} sm={12}>
              <TextField
                autoFocus
                margin = "dense"
                id = "observacao"
                label = "Observação"
                type = "text"
                variant = "outlined"
                multiline
                rows={3}
                value = {this.props.observacao}
                onChange={event => this.props.setObservacao(event.target.value)}
                fullWidth
              />
            </Grid>
          </Grid>
        </div>
      </FormDialog>
    );
  }
}

export default withStyles(styles)(VisitasForm);
