import React from 'react';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import NumberFormat from 'react-number-format';

import styles from './styles/DetailPedidoExterno';

class DetailPedidoExterno extends React.PureComponent {
  render(){
    const { pedido, classes, getProduto, getPrecoProduto } = this.props;
    let totalPedido = 0;
    return (
      <Grid container spacing={1} key={pedido.id}>
        <Grid item xs={12} sm={12}>
          <Grid container spacing={1} className={classes.conteudoContainer}>
            <Grid item xs={12} sm={6}>
              <Typography variant="body2" gutterBottom>
                <strong>Cliente: </strong>
                {this.props.getCliente(pedido.cliente)}
              </Typography>
            </Grid>
            <Grid item xs={12} sm={6}>
              <Typography variant="body2" gutterBottom>
                <strong>Status: </strong>
                {this.props.getPedidoStatus(pedido.pedidostatus)}
              </Typography>
            </Grid>
            <Grid item xs={12} sm={12}>
              <Grid container className={classes.conteudoProdutoContainer}>
                <Grid item xs={6} sm={6} className={classes.cabecalhoLabel}>
                    Produto
                </Grid>
                <Grid item xs={2} sm={2} className={classes.totalLabel}>
                  Quantidade
                </Grid>
                <Grid item xs={4} sm={4} className={classes.totalLabel}>
                    Total
                </Grid>
                {pedido.produtos.map(pedidoProduto => {
                  const precoProduto = getPrecoProduto(pedidoProduto.produto) * pedidoProduto.quantidade;
                  totalPedido += precoProduto;

                  return (
                    <Grid item xs={12} sm={12} key={pedidoProduto.id}>
                      <Grid container className={classes.linhaProdutoContainer}>
                        <Grid item xs={6} sm={6}>
                          <Typography variant="body2" gutterBottom>
                            {getProduto(pedidoProduto.produto)}
                          </Typography>
                        </Grid>
                        <Grid item xs={2} sm={2} className={classes.totalValue}>
                          <Typography variant="body2" gutterBottom>
                            {pedidoProduto.quantidade}
                          </Typography>
                        </Grid>
                        <Grid item xs={4} sm={4} className={classes.totalValue}>
                          <Typography variant="body2" gutterBottom>
                            <NumberFormat value={precoProduto.toFixed(2)} displayType='text' thousandSeparator="." decimalSeparator="," isNumericString prefix="R$"/>
                          </Typography>
                        </Grid>
                      </Grid>
                    </Grid>
                  );
                })}
                <Grid item xs={8} sm={8} className={classes.totalLabel}>
                </Grid>
                <Grid item xs={4} sm={4} className={classes.totalValue}>
                  <Typography variant="body2" gutterBottom>
                    <NumberFormat value={totalPedido.toFixed(2)} displayType='text' thousandSeparator="." decimalSeparator="," isNumericString prefix="R$"/>
                  </Typography>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
  	);
  }
}

export default withStyles(styles)(DetailPedidoExterno);
