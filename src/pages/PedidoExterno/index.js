import React from 'react';
import PedidoExterno from './PedidoExterno';
import CssBaseline from '@material-ui/core/CssBaseline';

export default function PedidosIndex(){
  return (
    <>
      <CssBaseline />
      <PedidoExterno/>
    </>
	);
}
