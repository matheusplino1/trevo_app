import React from 'react';
import { withRouter } from "react-router-dom";
import { withStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';

import api from '../../services/api';
import withMessageHandler from '../../hoc/withMessageHandler/withMessageHandler';
import DetailPedidoExterno from './DetailPedidoExterno';
import styles from './styles/PedidoExterno';

const objetoPadrao = { id: null, cliente : "", pedidostatus : "", produtos: []};
const url = '/pedidos';

class PedidoExterno extends React.Component {

  constructor(props){
    super(props);

    this.state = {
      pedido: {},
      pedidos: [],
      arrClientes: [],
      arrProdutos: [],
      arrPedidoStatus: [],
    }
  }

  setCliente = (value) => this.setState({ cliente : value });
  setPedidoStatus = (value) => this.setState({ pedidostatus : value });
  setProduto = (value) => this.setState({ produto : value });
  getPedidoStatus = (id) => this.state.arrPedidoStatus.filter(model => model.id === id)[0].descricao;
  getCliente = (id) => this.state.arrClientes.filter(model => model.id === id)[0].nome;
  getProduto = (id) => this.state.arrProdutos.filter(model => model.id === id)[0].descricao;
  getPrecoProduto = (id) => this.state.arrProdutos.filter(model => model.id === id)[0].preco;
  setCurrentObj = (data) => this.setState({ currentObj : {...data}  });
  handleOpen = () => {};

  componentDidMount(){
    const arrPromises = [];

    arrPromises.push(api.request({
      url: '/usuarios',
      method: 'get'
    }).then(response => {
      const arrClientes = response.data.items;
      this.setState({ arrClientes });
    }).catch(this.props.handleError));

    arrPromises.push(api.request({
      url: '/pedidostatus',
      method: 'get'
    }).then(response => {
      const arrPedidoStatus = response.data.items;
      this.setState({ arrPedidoStatus });
    }).catch(this.props.handleError));

    arrPromises.push(api.request({
      url: '/produtos',
      method: 'get'
    }).then(response => {
      const arrProdutos = response.data.items;
      this.setProduto(arrProdutos[0].id || null);
      this.setState({ arrProdutos });
    }).catch(this.props.handleError));

    Promise.all(arrPromises).then(() => {
      this.setCurrentObj({...objetoPadrao});
      const id = this.props.match.params.id;

      api.request({
        url : `${url}/${id}`,
        method: 'get'
      }).then(response => {
        let pedido = response.data;
        pedido.produtos = [];

        api.request({
          url: `pedidosprodutos?pedido=${pedido.id}`,
          method: 'get'
        }).then(response => {
          const { items } = response.data;
          if (items.length > 0) {
            pedido.produtos = items;
          }

          this.setState({ pedido });
          this.setState({ pedidos : [ pedido ] });
        }).catch(this.props.handleError);
      }).catch(this.props.handleError);
    });
  }

  render() {
    const classes = this.props.classes;

    return (
      <Container fixed>
        <div className={classes.pedidoContainer}>
          {this.state.pedidos.map(pedido => {
            return (
              <DetailPedidoExterno
                key={this.state.pedido.id}
                pedido={this.state.pedido}
                setCurrentObjById={this.setCurrentObjById}
                handleOpen={this.handleOpen}
                getPedidoStatus={this.getPedidoStatus}
                getCliente={this.getCliente}
                getProduto={this.getProduto}
                getPrecoProduto={this.getPrecoProduto}
                handleOpenProduto={this.handleOpenProduto}
                setCurrentPedidoProduto={this.setCurrentPedidoProduto}
                setOpenConfirm={this.setOpenConfirm}
              />
            );
          })}
        </div>
      </Container>
    );
  }
}

export default withStyles(styles)(withRouter(withMessageHandler(PedidoExterno)));
