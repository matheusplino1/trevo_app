const styles = theme => ({
  pedidoContainer: {
    width: '100%',
    height: '100%',
    padding: theme.spacing(0.5),
    marginTop: theme.spacing(2),
    borderRadius: 5,
    backgroundColor: 'white',
  }
});

export default styles;
