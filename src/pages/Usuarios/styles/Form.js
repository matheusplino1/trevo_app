const styles = theme => ({
  root: {
    flexGrow: 1,
    overflow: 'hidden'
  },
  formControl: {
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
});

export default styles;
