import React, { PureComponent } from 'react';
import { withRouter } from 'react-router-dom';
import Container from '@material-ui/core/Container';

import api from '../../services/api';
import loginService from '../../services/loginService';
import withMessageHandler from '../../hoc/withMessageHandler/withMessageHandler';

import ActionTable from '../../components/ActionTable';
import ConfirmDialog from '../../components/ConfirmDialog';
import Usuario from '../../models/Usuario';
import UsuariosForm from './Form';

const url = '/usuarios';

class Usuarios extends PureComponent {
  constructor(props){
    super(props);

    if(!loginService.verificaLogin()){
      props.history.push('/');
    }

    this.state = {
      open: false,
      openConfirm: false,
      currentObj: new Usuario(),
      data: [],
      columns: [
        { title: 'Nome', field: 'nome', render: rowData => rowData.getNome() },
        { title: 'Email', field: 'email', render: rowData => rowData.getEmail() },
        { title: 'Endereço', field: 'logradouro', render: rowData => rowData.getLogradouro() },
        { title: 'CEP', field: 'cep', render: rowData => rowData.getCep() },
        { title: 'Número', field: 'numero', render: rowData => rowData.getNumero() },
        { title: 'Cidade', field: 'cidade', render: rowData => rowData.getCidade() },
        { title: 'Complemento', field: 'complemento', render: rowData => rowData.getComplemento() }
      ]
    }
  }

  componentDidMount(){
    api.request({
      url: `${url}?perfil=${this.props.perfil}`,
      method: 'get'
    }).then(
      response => this.setState({ data: Usuario.fromDataRequest(response.data.items) })
    ).catch(this.props.handleError);
  }

  setOpen = (value) => this.setState({ open : value });
  setOpenConfirm = (value) => this.setState({ openConfirm : value });
  setCurrentObj = (value) => this.setState({ currentObj : value });
  setData = (value) => this.setState({ data : value });

  handleClickOpen = (event, rowData) => {
    this.setCurrentObj(rowData);
    this.setOpen(true);
  };

  handleClickOpenAdd = (event, rowData) => {
    const newObj = new Usuario();
    this.setCurrentObj(newObj);
    this.setOpen(true);
  };

  handleClose = (event, usuario) => {
    if (event.target.id === 'confirm' || event.target.parentElement.id === 'confirm') {
      const
        dados = [...this.state.data],
        currentObj = this.state.currentObj,
        urlRota = currentObj.getId() === null ? url : `${url}/${currentObj.getId()}`,
        method = currentObj.getId() === null ? `post` : `put`;

      let index, formData;

      usuario.setPerfil(this.props.perfil);
      usuario.setId(this.state.currentObj.getId());
      formData = usuario.getFormData();
      if (usuario.getId() !== null) {
        index = this.state.data.indexOf(this.state.currentObj);
      }
      this.setCurrentObj(usuario);

      api.request({
        url : urlRota,
        method,
        data: formData
      }).then(response => {
        if (index >= 0) {
          dados[index] = usuario;
        } else {
          usuario.setId(response.data.id);
          dados.push(usuario);
        }
        this.setData(dados);
        this.props.handleSuccess('Usuário salvo com sucesso!');
      }).catch(this.props.handleError);
    }

    this.setOpen(false);
  };

  handleDelete = (event, rowData) => {
    this.setCurrentObj(rowData);
    this.setOpenConfirm(true);
  }

  handleCloseConfirm = (event) => {
    if(event.target.id === 'confirm' || event.target.parentElement.id === 'confirm'){
      api.request({
        url: `${url}/${this.state.currentObj.getId()}`,
        method: 'delete'
      }).then(response => {
        this.setData(
          this.state.data.filter(dado => dado.getId() !== this.state.currentObj.getId())
        );
        this.props.handleSuccess('Usuário deletado com sucesso!');
      }).catch(this.props.handleError);
    }
    this.setOpenConfirm(false);
  }

	render() {
    return (
      <Container fixed>
        <ActionTable
          title={'Usuarios'}
          columns={this.state.columns}
          data={this.state.data}
          setData={this.setData}
          onAdd={this.handleClickOpenAdd}
          onEdit={this.handleClickOpen}
          onDelete={this.handleDelete}
        />
        <UsuariosForm
          currentObj={this.state.currentObj}
          open={this.state.open}
          handleClose={this.handleClose}
          isModal={true}
        />
        <ConfirmDialog
          open={this.state.openConfirm}
          handleClose={this.handleCloseConfirm}
          title={"Tem certeza que deseja excluir o registro?"}
        />
      </Container>
    );
	};
}

export default withRouter(withMessageHandler(Usuarios));
