import React from 'react';
import Menu from '../Common/Menu';
import CssBaseline from '@material-ui/core/CssBaseline';
import Usuarios from './Usuarios';

const perfil = 1;

export default function UsuariosIndex({ history }) {
  return (
    <>
      <CssBaseline />
      <Menu/>
      <Usuarios perfil = {perfil}/>
    </>
	);
}
