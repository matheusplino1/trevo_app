import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';

import FormDialog from '../../components/FormDialog';
import Usuario from '../../models/Usuario';

import styles from './styles/Form';

class UsuariosForm extends React.PureComponent {

  constructor(props) {
    super(props);

    this.state = {
      nome: '',
      email: '',
      senha: '',
      logradouro: '',
      cidade: '',
      cep: '',
      numero: '',
      complemento: ''
    }
  }

  setNome = (value) => this.setState({ nome : value });
  setEmail = (value) => this.setState({ email : value });
  setSenha = (value) => this.setState({ senha : value });
  setLogradouro = (value) => this.setState({ logradouro : value });
  setCidade = (value) => this.setState({ cidade : value });
  setCep = (value) => this.setState({ cep : value });
  setNumero = (value) => this.setState({ numero : value });
  setComplemento = (value) => this.setState({ complemento : value });

  onEnter = () => {
    const currentObj = this.props.currentObj;

    this.setNome(currentObj.getNome() || '');
    this.setEmail(currentObj.getEmail() || '');
    this.setSenha(currentObj.getSenha() || '');
    this.setLogradouro(currentObj.getLogradouro() || '');
    this.setCidade(currentObj.getCidade() || '');
    this.setCep(currentObj.getCep() || '');
    this.setNumero(currentObj.getNumero() || '');
    this.setComplemento(currentObj.getComplemento() || '');
  }

  onClose = (event) => {
    const usuario = Usuario.fromData(this.state);
    this.props.handleClose(event, usuario);
  }

  render(){
    return (
      <>
        { (this.props.isModal === true) ?
            <FormDialog open={this.props.open} onEnter={this.onEnter} handleclose={this.onClose} title={"Salvar Usuário"}>
              { this.getForm() }
            </FormDialog>
            : this.getForm()
        }
      </>
    );
  }

  getForm() {
    return (
      <div className={this.props.classes.root}>
        <Grid container spacing={1}>
          <Grid item xs={12} sm={12}>
            <TextField
              autoFocus
              margin = "dense"
              id = "nome"
              label = "Nome"
              type = "text"
              variant = "outlined"
              value = {this.state.nome}
              onChange={event => this.setNome(event.target.value)}
              fullWidth
            />
          </Grid>
          <Grid item xs={12} sm={12}>
            <TextField
              margin = "dense"
              id = "email"
              label = "Email"
              type = "email"
              variant = "outlined"
              value = {this.state.email}
              onChange={event => this.setEmail(event.target.value)}
              fullWidth
            />
          </Grid>
          <Grid item xs={12} sm={12}>
            <TextField
              margin = "dense"
              id = "senha"
              label = "Senha"
              type="password"
              variant = "outlined"
              value = {this.state.senha}
              onChange={event => this.setSenha(event.target.value)}
              fullWidth
            />
          </Grid>
          <Grid item xs={12} sm={12}>
            <TextField
              margin = "dense"
              id = "logradouro"
              label = "Endereço"
              type = "text"
              variant = "outlined"
              value = {this.state.logradouro}
              onChange={event => this.setLogradouro(event.target.value)}
              fullWidth
            />
          </Grid>
          <Grid item xs={12} sm={12}>
            <TextField
              margin = "dense"
              id = "cidade"
              label = "Cidade"
              type = "text"
              variant = "outlined"
              value = {this.state.cidade}
              onChange={event => this.setCidade(event.target.value)}
              fullWidth
            />
          </Grid>
          <Grid item xs={6} sm={6}>
            <TextField
              margin = "dense"
              id = "cep"
              label = "CEP"
              type = "text"
              variant = "outlined"
              value = {this.state.cep}
              onChange={event => this.setCep(event.target.value)}
              fullWidth
            />
          </Grid>
          <Grid item xs={6} sm={6}>
            <TextField
              margin = "dense"
              id = "numero"
              label = "Número"
              type = "text"
              variant = "outlined"
              value = {this.state.numero}
              onChange={event => this.setNumero(event.target.value)}
              fullWidth
            />
          </Grid>
          <Grid item xs={12} sm={12}>
            <TextField
              margin = "dense"
              id = "complemento"
              label = "Complemento"
              type = "text"
              variant = "outlined"
              value = {this.state.complemento}
              onChange={event => this.setComplemento(event.target.value)}
              fullWidth
            />
          </Grid>
        </Grid>
      </div>
    );
  }
}

export default withStyles(styles)(UsuariosForm);
