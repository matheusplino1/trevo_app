import React, { useState } from 'react';
import Avatar from '@material-ui/core/Avatar';
import CssBaseline from '@material-ui/core/CssBaseline';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import Form from './Form';
import SnackBar from '../../components/SnackBar';
import Copyright from '../../components/Copyright';

import api from '../../services/api';
import loginService from '../../services/loginService';
import useStyles from './styles/index';

export default function Login({ history }) {
  const classes = useStyles();
  const [email, setEmail] = useState('');
  const [senha, setSenha] = useState('');
  const [open, setOpen] = useState(false);
  const [message, setMessage] = useState('');

  if(!!loginService.verificaLogin()){
    history.push('/dashboard');
  }

  const handleClick = (error) => {
    let mensagem = error.message;

    if (error.message === 'Network Error'){
      mensagem = "Não foi possível conectar com a API.";
    } else if (!!error.response && !!error.response.data && !!error.response.data.message) {
      mensagem = error.response.data.message;
    }

    setMessage(mensagem);
    setOpen(true);
  };

  const handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    setOpen(false);
  };

  async function handleSubmit(event) {
    event.preventDefault();

    api.post('/authenticate', { email, senha }).then(response => {
      const { accessToken } = response.data;
      localStorage.setItem('token', accessToken);
      api.defaults.headers.common['Authorization'] = `Bearer ${accessToken}`;

      api.request({
        url: '/getmenu',
        method: 'post'
      }).then(response => {
        let grupos = [];
        response.data.forEach(item => {
          if(!grupos.find(variavel => variavel.grupo === item.grupo)){
            grupos.push({
              grupo: item.grupo
            });
          }
        });
        grupos = grupos.map((grupo) => {
          grupo.items = response.data.filter(item => item.grupo === grupo.grupo);
          return grupo;
        });

        localStorage.setItem('menu', JSON.stringify(grupos));
        history.push('/dashboard');
      }).catch(handleClick);
    }).catch(handleClick);
  }

	return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar src="logo192.png"/>
        <Typography component="h1" variant="h5">
          Login
        </Typography>
        <Form
          handleSubmit={handleSubmit}
          email={email}
          senha={senha}
          setEmail={setEmail}
          setSenha={setSenha}
        />
      </div>
      <Box mt={8}>
        <Copyright />
      </Box>
      <div>
        <SnackBar
          variant="error"
          className={classes.margin}
          message={message}
          handleClose={handleClose}
          open={open}
        />
      </div>
    </Container>
	);
}
