import React, { PureComponent } from 'react';
import { withStyles } from '@material-ui/core/styles';
import TablePagination from '@material-ui/core/TablePagination';
import Grid from '@material-ui/core/Grid';

import ProdutoItem from './ProdutoItem';
import ProdutoPesquisa from './ProdutoPesquisa';
import styles from './styles/ProdutosTable';

class ProdutosTable extends PureComponent {
  constructor(props){
    super(props);

    this.state = {
      page: 0,
      linhasPorPagina: 10,
      texto: ''
    }
  }

  componentDidMount(){
    this.atualizarProdutos(
      this.state.page,
      this.state.linhasPorPagina,
      this.state.texto
    );
  }

  setLinhasPorPagina = (value) => this.setState({ linhasPorPagina : value });
  setPage = (value) => this.setState({ page : value });
  setTexto = (value) => this.setState({ texto : value });
  atualizarProdutos = (pagina, linhasPorPagina, texto) => {
    this.props.atualizarProdutos(pagina, linhasPorPagina, texto);
  }

  handleChangePage = (event, newPage) => {
    this.setPage(newPage);
    this.atualizarProdutos(
      newPage,
      this.state.linhasPorPagina,
      this.state.texto
    );
  }

  handleChangeLinhasPorPagina = (event) => {
    const linhasPorPagina = parseInt(event.target.value, 10);
    this.setLinhasPorPagina(linhasPorPagina);
    this.setPage(0);
    this.atualizarProdutos(
      0,
      linhasPorPagina,
      this.state.texto
    );
  }

  handleFind = (texto) => {
    this.setTexto(texto);
    this.atualizarProdutos(
      this.state.page,
      this.state.linhasPorPagina,
      texto
    );
  }

  render(){
    const { classes } = this.props;
    return (
      <div className={classes.container}>
        <ProdutoPesquisa handleFind={this.handleFind}/>
        <Grid container spacing={1} className={classes.corpo}>
          {
            this.props.produtos.map(produto => <ProdutoItem key={produto.id} produto={produto}/>)
          }
        </Grid>
        <Grid container spacing={1} className={classes.rodape}>
          <TablePagination
            className={classes.paginacao}
            component="div"
            count={this.props.count}
            page={this.state.page}
            onChangePage={this.handleChangePage}
            rowsPerPage={this.state.linhasPorPagina}
            onChangeRowsPerPage={this.handleChangeLinhasPorPagina}
            SelectProps={{
              inputProps: { 'aria-label': 'Linhas por Página' },
              native: true,
            }}
          />
        </Grid>
      </div>
    );
  }
}

export default withStyles(styles)(ProdutosTable);
