import React from 'react';
import Grid from '@material-ui/core/Grid';

import loginService from '../../services/loginService';
import useStyles from './styles/ProdutoItem';
import camera from '../../assets/camera-fotografica.svg';

export default function ProdutoItem(props) {
  const
    { produto } = props,
    classes = useStyles(),
    thumbnail = produto.thumbnail,
    thumbnailUrl = `${loginService.apiUrl}/files/${thumbnail}`,
    url = (!!thumbnail) ? thumbnailUrl : camera;

  return (
    <Grid item sm={6} xs={12} className={classes.produto}>
      <Grid container>
        <Grid item sm={3} xs={4} className={classes.thumbnailContainer}>
          <img src={url} className={classes.thumbnail} alt={`Foto do produto ${produto.descricao}`}/>
        </Grid>
        <Grid item sm={9} xs={8} className={classes.descricaoContainer}>
          <Grid container>
            <Grid item sm={6} xs={6} className={classes.thumbnailContainer}>
              {`${produto.descricao} `}
            </Grid>
            <Grid item sm={6} xs={6} className={classes.thumbnailContainer}>
              {` Preço: ${produto.preco} `}
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  );
}
