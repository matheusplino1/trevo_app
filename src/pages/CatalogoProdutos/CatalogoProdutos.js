import React, { PureComponent } from 'react';
import Container from '@material-ui/core/Container';
import { withRouter } from 'react-router-dom';
import Grid from '@material-ui/core/Grid';
import Divider from '@material-ui/core/Divider';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';

import withMessageHandler from '../../hoc/withMessageHandler/withMessageHandler';
import loginService from '../../services/loginService';
import api from '../../services/api';

import styles from './styles/CatalogoProdutos';
import ProdutosTable from './ProdutosTable';
import Produto from '../../models/Produto';

const url = '/produtos';

class CatalogoProdutos extends PureComponent {

  constructor(props){
    super(props);

    if(!loginService.verificaLogin()){
      props.history.push('/');
    }

    this.title = 'Catálogo de Produtos';
    this.state = {
      count: 0,
      produtos: []
    }
  }

  setProdutos = (value) => this.setState({ produtos : value });
  setCount = (value) => this.setState({ count : value });

  atualizarProdutos = (pagina, linhasPorPagina, texto) => {
    let newUrl = `${url}?_page=${pagina + 1}&_pagesize=${linhasPorPagina}`;
    if(texto !== ''){
      newUrl += `&like=descricao,${texto}`;
    }

    api.request({
      url : newUrl,
      method: 'get'
    })
    .then(response => {
      this.setProdutos(Produto.fromDataRequest(response.data.items));
      this.setCount(response.data.count);
    }).catch(this.props.handleError);
  }

	render() {
    const { classes } = this.props;
    const { page, produtos, count } = this.state;

    return (
      <Container fixed className={classes.root}>
        <Grid container spacing={0}>
          <Grid item xs={12} key={'cabecalho'}>
            <Grid container spacing={1}  className={classes.cabecalho}>
              <Grid item xs={12} sm={12} key={'cabecalho_label'}>
                <Typography variant="h5" gutterBottom>
                  <center>Catálogo de produtos</center>
                </Typography>
              </Grid>
            </Grid>
            <Divider variant="middle" className={classes.divider}/>
            <ProdutosTable
              produtos={produtos}
              atualizarProdutos={this.atualizarProdutos}
              count={count}
              page={page}
            />
          </Grid>
        </Grid>
      </Container>
    );
	};
}

export default withStyles(styles)(withRouter(withMessageHandler(CatalogoProdutos)));
