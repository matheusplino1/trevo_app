const styles = theme => ({
  root: {
    flexGrow: 1,
    overflow: 'hidden',
    marginBottom: theme.spacing(5)
  },
  cabecalho: {
    padding: theme.spacing(2,0,2),
    borderRadius: 5,
    backgroundColor: 'white',
  },
  divider: {
    opacity: '20%'
  },
});

export default styles;
