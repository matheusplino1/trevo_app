import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  produto: {
    borderRadius: '5px',
    border: '1px',
    boxShadow: '0 0 1px lightgrey'
  },
  thumbnail: {
    width: '100px',
    height: '100px',
    display: 'block',
    margin: 'auto',
    padding: theme.spacing(1),
  },
  descricaoContainer: {
    marginTop: 'auto',
    marginBottom: 'auto',
  },
}));

export default useStyles;
