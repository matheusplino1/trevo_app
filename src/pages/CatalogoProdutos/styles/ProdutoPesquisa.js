import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
  pesquisaContainer : {
    padding: theme.spacing(1,2,1,2),
    borderRadius: 5,
    backgroundColor: 'white',
  },
  buttonContainer : {
    marginTop: 'auto',
    marginBottom: 'auto',
  },
}));

export default useStyles;
