const styles = theme => ({
  container : {
    borderRadius: 5,
    backgroundColor: 'white',
    color: 'black',
  },
  corpo: {
    padding: theme.spacing(0,0,2),
    borderRadius: 5,
    backgroundColor: 'white',
  },
  rodape: {
    padding: theme.spacing(0,0,0),
    borderRadius: 5,
    backgroundColor: 'white',
  },
  paginacao: {
    margin: 'auto',
  },
});

export default styles;
