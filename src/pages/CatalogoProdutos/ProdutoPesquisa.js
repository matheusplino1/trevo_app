import React, {useState} from 'react';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';

import useStyles from './styles/ProdutoPesquisa';

export default function ProdutoItem(props) {
  const
    classes = useStyles(),
    [texto, setTexto] = useState(''),
    handleClick = (event) => props.handleFind(texto);

  return (
    <Grid container spacing={1} className={classes.pesquisaContainer}>
      <Grid item xs={8} sm={3}>
        <TextField
          margin = "dense"
          id = "texto"
          label = "Pesquisar"
          type = "text"
          variant = "outlined"
          value = {texto}
          onChange={event => setTexto(event.target.value)}
          fullWidth
        />
      </Grid>
      <Grid item xs={1} sm={1} className={classes.buttonContainer}>
        <Button onClick={handleClick} id="buscar" variant='contained' color="primary">
          Pesquisar
        </Button>
      </Grid>
    </Grid>
  );
}
