import React from 'react';
import Menu from '../Common/Menu';
import CatalogoProdutos from './CatalogoProdutos';
import CssBaseline from '@material-ui/core/CssBaseline';

export default function CatalogoProdutosIndex(){
  return (
    <>
      <CssBaseline />
      <Menu/>
      <CatalogoProdutos/>
    </>
	);
}
