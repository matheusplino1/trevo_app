import React from 'react';
import Menu from '../Common/Menu';
import CssBaseline from '@material-ui/core/CssBaseline';
import EditUser from './Edit';

export default function EditUsuario({ history }) {
  return (
    <>
      <CssBaseline/>
      <Menu/>
      <EditUser/>
    </>
	);
}
