const styles = theme => ({
  container: {
    width: '100%',
    height: '100%',
    maxWidth: '600px',
    padding: theme.spacing(2,2),
    borderRadius: 5,
    backgroundColor: 'white',
  },
  buttonContainer: {
    padding: theme.spacing(1,0,0,0),
    textAlign: 'center'
  }
});

export default styles;
