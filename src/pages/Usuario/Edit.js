import React from 'react';
import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';
import { withRouter } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';

import styles from './styles/Edit';
import api from '../../services/api';
import loginService from '../../services/loginService';
import withMessageHandler from '../../hoc/withMessageHandler/withMessageHandler';

import UsuariosForm from '../Usuarios/Form';

const url = '/usuarios';

class EditUser extends React.PureComponent{
  constructor(props){
    super(props);

    if(!loginService.verificaLogin()){
      props.history.push('/');
    }

    this.state = {
      nome : "",
      email : "",
      senha: "",
      logradouro : "",
      cep : "",
      numero : "",
      cidade : "",
      complemento :  ""
    };
  }

  componentDidMount(){
    api.request({
      url: `${url}/getUserData`,
      method: 'get'
    }).then(response => {
      const usuario = response.data;
      this.setState(usuario);
    }).catch(erro => console.log(erro));
  }

  render(){
    const classes = this.props.classes;

    return(
      <Container fixed className={classes.container}>
        <UsuariosForm
          nome={this.state.nome}
          email={this.state.email}
          logradouro={this.state.logradouro}
          cep={this.state.cep}
          numero={this.state.numero}
          cidade={this.state.cidade}
          complemento={this.state.complemento}
          senha={this.state.senha}
          setNome={this.setNome}
          setPerfil={this.setPerfil}
          setSenha={this.setSenha}
          setEmail={this.setEmail}
          setLogradouro={this.setLogradouro}
          setCep={this.setCep}
          setNumero={this.setNumero}
          setCidade={this.setCidade}
          setComplemento={this.setComplemento}
        />
        <Container className={classes.buttonContainer}>
          <Button onClick={this.handleClose} id="confirm" color="primary" type="submit" fullWidth >
            Salvar
          </Button>
        </Container>
      </Container>
    );
  }

  setNome = (value) => {
  	this.setState({ nome: value });
  }

  setEmail = (value) => {
  	this.setState({ email: value });
  }

  setLogradouro = (value) => {
  	this.setState({ logradouro: value });
  }

  setCep = (value) => {
  	this.setState({ cep: value });
  }

  setNumero = (value) => {
  	this.setState({ numero: value });
  }

  setCidade = (value) => {
  	this.setState({ cidade: value });
  }

  setComplemento = (value) => {
  	this.setState({ complemento: value });
  }

  setSenha = (value) => {
    this.setState({ senha: value });
  }

  handleClose = () => {
    const objeto = {};
    objeto.id = this.state.id;
    objeto.nome = this.state.nome;
    objeto.email = this.state.email;
    objeto.logradouro = this.state.logradouro;
    objeto.cep = this.state.cep;
    objeto.numero = this.state.numero;
    objeto.cidade = this.state.cidade;
    objeto.complemento = this.state.complemento;

    if(this.state.senha && this.state.senha !== ''){
      objeto.senha = this.state.senha;
    }

    api.request({
      url: `${url}/${this.state.id}`,
      method: `put`,
      data: objeto
    }).then(response => {
      this.props.handleSuccess('Usuario salvo com sucesso!');
    }).catch(this.props.handleError);
  }
}

export default withStyles(styles)(withRouter(withMessageHandler(EditUser)));
