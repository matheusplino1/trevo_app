import 'date-fns';
import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import FormDialog from '../../components/FormDialog';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Grid from '@material-ui/core/Grid';

import styles from './styles/Form';

class PedidosForm extends React.PureComponent {
  render(){
    return (
      <FormDialog open={this.props.open} handleclose={this.props.handleClose} title={"Salvar Pedido"}>
        <div className={this.props.classes.root}>
          <Grid container spacing={1}>
            <Grid item xs={12} sm={6}>
              <FormControl variant="outlined" fullWidth>
                <InputLabel id="select-cliente-label">
                  Cliente
                </InputLabel>
                <Select
                  labelId="select-cliente-label"
                  key="select-cliente"
                  id="select-cliente"
                  value={this.props.cliente}
                  onChange={event => this.props.setCliente(event.target.value)}
                >
                  {this.props.arrClientes && this.props.arrClientes.map(cliente => (
                    <MenuItem key={cliente.id} value={cliente.id}>{cliente.nome}</MenuItem>
                  ))}
                </Select>
              </FormControl>
            </Grid>
            <Grid item xs={12} sm={6}>
              <FormControl variant="outlined" fullWidth>
                <InputLabel id="select-perfil-label">
                  Status do Pedido
                </InputLabel>
                <Select
                  labelId="select-perfil-label"
                  key="select-perfil"
                  id="select-perfil"
                  value={this.props.pedidostatus}
                  onChange={event => this.props.setPedidoStatus(event.target.value)}
                >
                  {this.props.arrPedidoStatus && this.props.arrPedidoStatus.map(pedidostatus => (
                    <MenuItem key={pedidostatus.id} value={pedidostatus.id}>{pedidostatus.descricao}</MenuItem>
                  ))}
                </Select>
              </FormControl>
            </Grid>
          </Grid>
        </div>
      </FormDialog>
    );
  }
}

export default withStyles(styles)(PedidosForm);
