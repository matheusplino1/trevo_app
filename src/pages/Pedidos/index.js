import React from 'react';
import Menu from '../Common/Menu';
import Pedidos from './Pedidos';
import CssBaseline from '@material-ui/core/CssBaseline';

export default function PedidosIndex(){
  return (
    <>
      <CssBaseline />
      <Menu/>
      <Pedidos/>
    </>
	);
}
