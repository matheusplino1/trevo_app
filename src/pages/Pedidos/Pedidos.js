import React from 'react';
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import { withRouter } from 'react-router-dom';
import Button from '@material-ui/core/Button';
import AddBox from '@material-ui/icons/AddBox';
import Divider from '@material-ui/core/Divider';
import Container from '@material-ui/core/Container';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';

import api from '../../services/api';
import styles from './styles/Pedidos';
import loginService from '../../services/loginService';
import withMessageHandler from '../../hoc/withMessageHandler/withMessageHandler';

import PedidosForm from './Form';
import DetailPedido from './DetailPedido';
import ProdutosForm from './ProdutosForm';
import ConfirmDialog from '../../components/ConfirmDialog';

const objetoPadrao = { id: null, cliente : "", pedidostatus : "", produtos: []};
const url = '/pedidos';

class Pedidos extends React.PureComponent {

  constructor(props){
    super(props);

    if(!loginService.verificaLogin()){
      props.history.push('/');
    }

    this.state = {
      open: false,
      openConfirm: false,
      openProduto: false,
      pedidos: [],
    }

  }

  componentDidMount(){
    const arrPromises = [];

    arrPromises.push(api.request({
      url: '/usuarios',
      method: 'get'
    }).then(response => {
      const arrClientes = response.data.items;
      this.setState({ arrClientes });
    }).catch(this.props.handleError));

    arrPromises.push(api.request({
      url: '/pedidostatus',
      method: 'get'
    }).then(response => {
      const arrPedidoStatus = response.data.items;
      this.setState({ arrPedidoStatus });
    }).catch(this.props.handleError));

    arrPromises.push(api.request({
      url: '/produtos',
      method: 'get'
    }).then(response => {
      const arrProdutos = response.data.items;
      this.setProduto(arrProdutos[0].id || null);
      this.setState({ arrProdutos });
    }).catch(this.props.handleError));

    Promise.all(arrPromises).then(() => {
      this.setCurrentObj({...objetoPadrao});
      api.request({
        url,
        method: 'get'
      }).then(response => {
        let pedidos = response.data.items;
        const arrPromisesPedidos = [];

        pedidos.map(pedido => {
          pedido.produtos = [];
          return pedido;
        });

        pedidos.forEach(pedido => {
          arrPromisesPedidos.push(api.request({
            url: `pedidosprodutos?pedido=${pedido.id}`,
            method: 'get'
          }).then(response => {
            const { items } = response.data;
            if(items.length > 0){
              pedidos = pedidos.map(model => {
                if(model.id === pedido.id){
                  model.produtos = items;
                }
                return model;
              });
            }
          }).catch(this.props.handleError));
        });
        Promise.all(arrPromisesPedidos).then(() => this.setState({ pedidos }));
      }).catch(this.props.handleError);
    });
  }

  setOpen = (value) => this.setState({ open : value });
  setOpenConfirm = (value) => this.setState({ openConfirm : value });
  setOpenProduto = (value) => this.setState({ openProduto : value });
  setCliente = (value) => this.setState({ cliente : value });
  setPedidoStatus = (value) => this.setState({ pedidostatus : value });
  setProduto = (value) => this.setState({ produto : value });
  setQuantidade = (value) => this.setState({ quantidade : value });
  setOpenConfirm = (value) => this.setState({ openConfirm : value });
  setCurrentPedidoProduto = (value) => this.setState({ pedidoProduto : value });

  redirectPedidoExterno = (idPedido) => {
    this.props.history.push(`pedido/${idPedido}`);
  }

  setCurrentObj = (data) => {
  	this.setCliente(data.cliente);
  	this.setPedidoStatus(data.pedidostatus);
  	this.setState({ currentObj : {...data}  });
  }

  setCurrentObjById = (id) => {
    const obj = this.state.pedidos.filter(element => element.id === id);
    this.setCurrentObj(...obj);
  }

  getObjFromState = () => {
    const state = this.state;
    let obj;

    if(this.state.currentObj.id === null){
      obj = {...objetoPadrao};
    } else {
      obj = {...this.state.currentObj};
    }

    obj.cliente = state.cliente;
    obj.pedidostatus = state.pedidostatus;

    return obj;
  }

  getPedidoStatus = (id) => this.state.arrPedidoStatus.filter(model => model.id === id)[0].descricao;
  getCliente = (id) => this.state.arrClientes.filter(model => model.id === id)[0].nome;
  getProduto = (id) => this.state.arrProdutos.filter(model => model.id === id)[0].descricao;
  getPrecoProduto = (id) => this.state.arrProdutos.filter(model => model.id === id)[0].preco;

  handleOpen = event => {
    if(event.target.id === "add_pedido" || event.target.parentElement.id === "add_pedido"){
      this.setCurrentObj({...objetoPadrao});
    }
    this.setOpen(true);
  }

  handleOpenProduto = event => {
    this.setOpenProduto(true);
  }

  handleClose = (event) => {
    if(event.target.id === 'confirm' || event.target.parentElement.id === 'confirm'){
      const
        currentId = this.state.currentObj.id,
        pedido = this.getObjFromState();

      api.request({
        url: `${url}${!!currentId ? `/${currentId}` : ''}`,
        method: `${!!currentId ? `put` : 'post'}`,
        data: pedido
      }).then(response => {
        const pedidos = [...this.state.pedidos];
        if(!!currentId){
          let index;
          pedidos.forEach((model, idx) => { if (currentId === model.id) index = idx });
          pedidos[index] = pedido;
        } else {
          pedidos.push(pedido);
        }
        this.setState({ pedidos });
        this.setCurrentObj(pedido);
        this.props.handleSuccess('Pedido salvo com sucesso!');
      }).catch(this.props.handleError);
    }

    this.setOpen(false);
  };

  handleCloseProduto = (event) => {
    if(event.target.id === 'confirm' || event.target.parentElement.id === 'confirm'){
      const
        currentId = null,
        pedidoProduto = {
          pedido : this.state.currentObj.id,
          produto : this.state.produto,
          quantidade : this.state.quantidade
        };

      api.request({
        url: `${'pedidosprodutos'}${!!currentId ? `/${currentId}` : ''}`,
        method: `${!!currentId ? `put` : 'post'}`,
        data: pedidoProduto
      }).then(response => {
        let pedidos = this.state.pedidos.map(model => {
          if(model => model.id === this.state.currentObj.id) {
            model.produtos.push(pedidoProduto);
            model = {...model};
          }
          return model;
        });
        this.setState({ pedidos });
        this.props.handleSuccess('Produto salvo com sucesso!');
      }).catch(this.props.handleError);
    }

    this.setOpenProduto(false);
  }

  handleCloseConfirm = (event) => {
    if(event.target.id === 'confirm' || event.target.parentElement.id === 'confirm'){
      api.request({
        url: `pedidosprodutos/${this.state.pedidoProduto.id}`,
        method: 'delete'
      }).then(response => {
        let pedidos = this.state.pedidos.map(model => {
          if(model => model.id === this.state.pedidoProduto.pedido){
            model.produtos = model.produtos.filter(pedidoProduto => pedidoProduto.id !== this.state.pedidoProduto.id);
            model = {...model};
          }
          return model;
        })

        this.setState({ pedidos });
        this.props.handleSuccess('Produto excluído com sucesso!');
      }).catch(this.props.handleError);
    }
    this.setOpenConfirm(false);
  }

  render(){
    const classes = this.props.classes;

    return (
      <Container fixed>
        <div className={classes.root}>
          <Grid container spacing={0}>
            <Grid item xs={12}>
              <div className={classes.cabecalho}>
                <Grid container spacing={1}>
                  <Grid item xs={12} sm={10}>
                    <Typography variant="h5" gutterBottom>
                      <center>Pedidos</center>
                    </Typography>
                  </Grid>
                  <Grid item xs={12} sm={2}>
                    <Box className={classes.submit}>
                      <Button
                        fullWidth
                        id="add_pedido"
                        startIcon={<AddBox />}
                        variant="outlined"
                        color="primary"
                        onClick={this.handleOpen}
                      >
                        Novo Pedido
                      </Button>
                    </Box>
                  </Grid>
                </Grid>
              </div>
            </Grid>
            {this.state.pedidos.map(pedido => {
              return (
                <Grid item xs={12} key={pedido.id}>
                  <Divider variant="middle" key={`divider_${pedido.id}`} className={classes.divider}/>
                  <div className={classes.item}>
                    <DetailPedido
                      pedido={pedido}
                      setCurrentObjById={this.setCurrentObjById}
                      handleOpen={this.handleOpen}
                      getPedidoStatus={this.getPedidoStatus}
                      getCliente={this.getCliente}
                      getProduto={this.getProduto}
                      getPrecoProduto={this.getPrecoProduto}
                      handleOpenProduto={this.handleOpenProduto}
                      setCurrentPedidoProduto={this.setCurrentPedidoProduto}
                      setOpenConfirm={this.setOpenConfirm}
                      redirectPedidoExterno={this.redirectPedidoExterno}
                    />
                  </div>
                </Grid>
              );
            })}
          </Grid>
          <ConfirmDialog open={this.state.openConfirm} handleClose={this.handleCloseConfirm} title={"Tem certeza que deseja remover o Produto?"}/>
          <PedidosForm
            open={this.state.open}
            key={`pedidos_form`}
            handleClose={this.handleClose}
            cliente={this.state.cliente}
            pedidostatus={this.state.pedidostatus}
            setCliente={this.setCliente}
            setPedidoStatus={this.setPedidoStatus}
            arrUsuarios={this.state.arrUsuarios}
            arrClientes={this.state.arrClientes}
            arrPedidoStatus={this.state.arrPedidoStatus}
          />
          <ProdutosForm
            open={this.state.openProduto}
            key={`produtos_form`}
            handleClose={this.handleCloseProduto}
            produto={this.state.produto}
            quantidade={this.state.quantidade}
            setProduto={this.setProduto}
            setQuantidade={this.setQuantidade}
            arrProdutos={this.state.arrProdutos}
          />
        </div>
      </Container>
  	);
  }
}

export default withStyles(styles)(withRouter(withMessageHandler(Pedidos)));
