import 'date-fns';
import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import FormDialog from '../../components/FormDialog';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';

const styles = theme => ({
  root: {
    flexGrow: 1,
    overflow: 'hidden'
  },
});

class PedidosForm extends React.PureComponent {
  render(){
    return (
      <FormDialog open={this.props.open} handleclose={this.props.handleClose} title={"Adicionar Produto"}>
        <div className={this.props.classes.root}>
          <Grid container spacing={1}>
            <Grid item xs={12} sm={6}>
              <FormControl variant="outlined" fullWidth>
                <InputLabel id="select-produto-label">
                  Produto
                </InputLabel>
                <Select
                  labelId="select-produto-label"
                  key={"select-produto"}
                  id="select-produto"
                  value={this.props.produto}
                  onChange={event => this.props.setProduto(event.target.value)}
                >
                  {this.props.arrProdutos && this.props.arrProdutos.map(produto => (
                    <MenuItem key={produto.id} value={produto.id}>{produto.descricao}</MenuItem>
                  ))}
                </Select>
              </FormControl>
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                margin = "dense"
                id = "quantidade"
                label = "Quantidade"
                type = "text"
                variant = "outlined"
                value = {this.props.quantidade}
                onChange={event => this.props.setQuantidade(event.target.value)}
                fullWidth
              />
            </Grid>
          </Grid>
        </div>
      </FormDialog>
    );
  }
}

export default withStyles(styles)(PedidosForm);
