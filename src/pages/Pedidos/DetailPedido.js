import React from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Edit from '@material-ui/icons/Edit';
import AddBox from '@material-ui/icons/AddBox';
import Share from '@material-ui/icons/Share';
import Box from '@material-ui/core/Box';
import NumberFormat from 'react-number-format';
import DeleteIcon from '@material-ui/icons/Delete';
import IconButton from '@material-ui/core/IconButton';

import styles from './styles/DetailPedido';

class DetailPedido extends React.PureComponent {
  handleOpenProduto = event => {
    this.props.setCurrentObjById(this.props.pedido.id);
    this.props.handleOpenProduto(event);
  }

  handleOpenEdit = event => {
    this.props.setCurrentObjById(this.props.pedido.id);
    this.props.handleOpen(event);
  }

  handleDeletePedidoProduto = (event) => {
    const confirm = event.target.closest('#delete_produto');

    this.props.setCurrentPedidoProduto({
      id : +confirm.getAttribute("pedidoproduto"),
      pedido : +confirm.getAttribute("pedido"),
    });

    this.props.setOpenConfirm(true);
  }

  handleOpenShare = (event) => {
    this.props.redirectPedidoExterno(this.props.pedido.id);
  }

  render(){
    const { pedido, classes, getProduto, getPrecoProduto } = this.props;
    let totalPedido = 0;
    return (
      <>
        <CssBaseline />
        <Grid container spacing={1} key={pedido.id}>
          <Grid item xs={12} sm={9}>
            <Grid container spacing={1} className={classes.conteudoContainer}>
              <Grid item xs={12} sm={6}>
                <Typography variant="body2" gutterBottom>
                  <strong>Cliente: </strong>
                  {this.props.getCliente(pedido.cliente)}
                </Typography>
              </Grid>
              <Grid item xs={12} sm={6}>
                <Typography variant="body2" gutterBottom>
                  <strong>Status: </strong>
                  {this.props.getPedidoStatus(pedido.pedidostatus)}
                </Typography>
              </Grid>
              <Grid item xs={12} sm={12}>
                <Grid container className={classes.conteudoProdutoContainer}>
                  <Grid item xs={5} sm={5} className={classes.cabecalhoLabel}>
                      Produto
                  </Grid>
                  <Grid item xs={2} sm={2} className={classes.totalLabel}>
                    Quantidade
                  </Grid>
                  <Grid item xs={4} sm={4} className={classes.totalLabel}>
                      Total
                  </Grid>
                  <Grid item xs={1} sm={1}/>
                  {pedido.produtos.map(pedidoProduto => {
                    const precoProduto = getPrecoProduto(pedidoProduto.produto) * pedidoProduto.quantidade;
                    totalPedido += precoProduto;

                    return (
                      <Grid item xs={12} sm={12} key={pedidoProduto.id}>
                        <Grid container className={classes.linhaProdutoContainer}>
                          <Grid item xs={5} sm={5}>
                            <Typography variant="body2" gutterBottom>
                              {getProduto(pedidoProduto.produto)}
                            </Typography>
                          </Grid>
                          <Grid item xs={2} sm={2} className={classes.totalValue}>
                            <Typography variant="body2" gutterBottom>
                              {pedidoProduto.quantidade}
                            </Typography>
                          </Grid>
                          <Grid item xs={4} sm={4} className={classes.totalValue}>
                            <Typography variant="body2" gutterBottom>
                              <NumberFormat value={precoProduto.toFixed(2)} displayType='text' thousandSeparator="." decimalSeparator="," isNumericString prefix="R$"/>
                            </Typography>
                          </Grid>
                          <Grid item xs={1} sm={1}>
                            <IconButton
                              aria-label="delete"
                              className={classes.buttonDelete}
                              id="delete_produto"
                              pedido={pedidoProduto.pedido}
                              pedidoproduto={pedidoProduto.id}
                              onClick={this.handleDeletePedidoProduto}
                              disabled={pedido.pedidostatus > 1 ? true : false}
                            >
                              <DeleteIcon />
                            </IconButton>
                          </Grid>
                        </Grid>
                      </Grid>
                    );
                  })}
                  <Grid item xs={7} sm={7} className={classes.totalLabel}>
                  </Grid>
                  <Grid item xs={4} sm={4} className={classes.totalValue}>
                    <Typography variant="body2" gutterBottom>
                      <NumberFormat value={totalPedido.toFixed(2)} displayType='text' thousandSeparator="." decimalSeparator="," isNumericString prefix="R$"/>
                    </Typography>
                  </Grid>
                  <Grid item xs={1} sm={1} />
                </Grid>
              </Grid>
            </Grid>
          </Grid>
          <Grid item xs={12} sm={3}>
            <Grid container spacing={0} className={classes.buttonsContainer}>
              <Grid item xs={6} sm={6}>
                <Box className={classes.submit}>
                  <Button
                    fullWidth
                    id="add_produto"
                    startIcon={<AddBox />}
                    variant="outlined"
                    color="primary"
                    onClick={this.handleOpenProduto}
                    disabled={pedido.pedidostatus > 1 ? true : false}
                  >
                    Produto
                  </Button>
                </Box>
              </Grid>
              <Grid item xs={6} sm={6}>
                <Box className={classes.submit}>
                  <Button
                    fullWidth
                    id="edit_pedido"
                    startIcon={<Edit />}
                    variant="outlined"
                    color="primary"
                    onClick={this.handleOpenEdit}
                    disabled={pedido.pedidostatus > 1 ? true : false}
                  >
                    Editar
                  </Button>
                </Box>
              </Grid>
              <Grid item xs={12} sm={12}>
                <Box className={classes.submit}>
                  <Button
                    fullWidth
                    className={classes.buttonShare}
                    id="share_pedido"
                    startIcon={<Share />}
                    variant="outlined"
                    color="primary"
                    onClick={this.handleOpenShare}
                  >
                    Compartilhar
                  </Button>
                </Box>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </>
  	);
  }
}

export default withStyles(styles)(DetailPedido);
