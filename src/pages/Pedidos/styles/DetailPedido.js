const styles = theme => ({
  submit: {
    transform: "translateY(-50%)",
    top: "50%",
    position: "relative",
    marginRight: theme.spacing(2),
    marginLeft: theme.spacing(2),
  },
  conteudoProdutoContainer: {},
  linhaProdutoContainer: {},
  conteudoContainer: {
    padding: theme.spacing(2, 2, 1),
  },
  buttonsContainer: {
    height: "100%",
    width: "100%",
  },
  totalLabel: {
    fontWeight: 'bold',
    textAlign: 'right',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
  },
  totalValue: {
    textAlign: 'right'
  },
  cabecalhoLabel: {
    overflow: 'hidden',
    textOverflow: 'ellipsis',
    fontWeight: 'bold',
  },
  buttonDelete: {
    padding: 0,
    color: 'red',
    position: 'relative',
    transform: 'translateX(-50%)',
    left: '50%',
    opacity: '55%',
    bottom: '2px',
  },
  buttonShare: {
    marginTop: '10px',
  }
});

export default styles;
