const styles = theme => ({
  root: {
    flexGrow: 1,
    overflow: 'hidden',
    marginBottom: theme.spacing(5)
  },
  cabecalho: {
    width: '100%',
    height: '100%',
    padding: theme.spacing(2,0,2),
    borderRadius: 5,
    backgroundColor: 'white',
  },
  item: {
    width: '100%',
    height: '100%',
    padding: theme.spacing(0.5),
    paddingBottom: theme.spacing(2),
    borderRadius: 5,
    backgroundColor: 'white',
  },
  submit: {
    transform: "translateY(-50%)",
    top: "50%",
    position: "relative",
    marginRight: theme.spacing(2),
    marginLeft: theme.spacing(2),
  },
  divider: {
    opacity: '20%'
  },
  margin: {

  }
});

export default styles;
