import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import FormDialog from '../../components/FormDialog';
import Grid from '@material-ui/core/Grid';

const styles = theme => ({
  root: {
    flexGrow: 1,
    overflow: 'hidden'
  },
});

class ClientesForm extends React.PureComponent {
  render(){
    return (
      <FormDialog open={this.props.open} handleclose={this.props.handleClose} title={"Salvar Cliente"}>
        <div className={this.props.classes.root}>
          <Grid container spacing={1}>
            <Grid item xs={12}>
              <TextField
                autoFocus
                margin = "dense"
                id = "nome"
                label = "Nome"
                type = "text"
                variant = "outlined"
                value = {this.props.nome}
                onChange={event => this.props.setNome(event.target.value)}
                fullWidth
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                margin = "dense"
                id = "email"
                label = "Email"
                type = "text"
                variant = "outlined"
                value = {this.props.email}
                onChange={event => this.props.setEmail(event.target.value)}
                fullWidth
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                margin = "dense"
                id = "logradouro"
                label = "Endereço"
                type = "text"
                variant = "outlined"
                value = {this.props.logradouro}
                onChange={event => this.props.setLogradouro(event.target.value)}
                fullWidth
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                margin = "dense"
                id = "cidade"
                label = "Cidade"
                type = "text"
                variant = "outlined"
                value = {this.props.cidade}
                onChange={event => this.props.setCidade(event.target.value)}
                fullWidth
              />
            </Grid>
            <Grid item xs={6}>
              <TextField
                margin = "dense"
                id = "cep"
                label = "CEP"
                type = "text"
                variant = "outlined"
                value = {this.props.cep}
                onChange={event => this.props.setCep(event.target.value)}
                fullWidth
              />
            </Grid>
            <Grid item xs={6}>
              <TextField
                margin = "dense"
                id = "numero"
                label = "Número"
                type = "text"
                variant = "outlined"
                value = {this.props.numero}
                onChange={event => this.props.setNumero(event.target.value)}
                fullWidth
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                margin = "dense"
                id = "complemento"
                label = "Complemento"
                type = "text"
                variant = "outlined"
                value = {this.props.complemento}
                onChange={event => this.props.setComplemento(event.target.value)}
                fullWidth
              />
            </Grid>
          </Grid>
        </div>
      </FormDialog>
    );
  }
}

export default withStyles(styles)(ClientesForm);
