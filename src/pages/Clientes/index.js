import React from 'react';
import Menu from '../Common/Menu';
import Usuarios from '../Usuarios/Usuarios';
import CssBaseline from '@material-ui/core/CssBaseline';

const perfil = 2;

export default function ClientesIndex(){
  return (
    <>
      <CssBaseline />
      <Menu/>
      <Usuarios perfil={perfil}/>
    </>
	);
}
