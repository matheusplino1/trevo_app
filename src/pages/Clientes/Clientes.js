import React, { PureComponent } from 'react';
import { withRouter } from 'react-router-dom';
import Container from '@material-ui/core/Container';
import loginService from '../../services/loginService';
import ActionTable from '../../components/ActionTable';
import ConfirmDialog from '../../components/ConfirmDialog';
import ClientesForm from './Form';
import api from '../../services/api';

const objetoPadrao = { id: null, nome : "", email : "", logradouro : "", cep : "", numero : "", cidade : "", complemento :  ""};
const url = '/clientes';

class Clientes extends PureComponent {
  constructor(props){
    super(props);

    if(!loginService.verificaLogin()){
      props.history.push('/');
    }

    this.state = {
      open: false,
      openConfirm: false,
      currentObj:{...objetoPadrao},
      nome : "",
      email : "",
      logradouro : "",
      cep : "",
      numero : "",
      cidade : "",
      complemento :  "",
      columns: [
        { title: 'ID', field: 'id' },
        { title: 'Nome', field: 'nome' },
        { title: 'Email', field: 'email'},
        { title: 'Endereço', field: 'logradouro'},
        { title: 'CEP', field: 'cep'},
        { title: 'Número', field: 'numero'},
        { title: 'Cidade', field: 'cidade'},
        { title: 'Complemento', field: 'complemento'}
      ],
      data: []
    }
  }

  componentDidMount(){
    api.request({
      url,
      method: 'get'
    })
    .then(response => this.setState({ data: response.data.items }))
    .catch(erro => console.log(erro));
  }

  setOpen = (value) => {
    this.setState({ open : value });
  }

  setOpenConfirm = (value) => {
    this.setState({ openConfirm : value });
  }

  setCurrentObj = (value) => {
    this.setNome(value.nome);
  	this.setEmail(value.email);
  	this.setLogradouro(value.logradouro);
  	this.setCep(value.cep);
  	this.setNumero(value.numero);
  	this.setCidade(value.cidade);
  	this.setComplemento(value.complemento);
    this.setState({ currentObj : value });
  }

  setData = (value) => {
    this.setState({ data : value });
  }

  setNome = (value) => {
  	this.setState({ nome: value });
  }

  setEmail = (value) => {
  	this.setState({ email: value });
  }

  setLogradouro = (value) => {
  	this.setState({ logradouro: value });
  }

  setCep = (value) => {
  	this.setState({ cep: value });
  }

  setNumero = (value) => {
  	this.setState({ numero: value });
  }

  setCidade = (value) => {
  	this.setState({ cidade: value });
  }

  setComplemento = (value) => {
  	this.setState({ complemento: value });
  }

  handleClickOpen = (event, rowData) => {
    this.setCurrentObj(rowData);
    this.setOpen(true);
  };

  handleClickOpenAdd = (event, rowData) => {
    this.setCurrentObj({...objetoPadrao});
    this.setOpen(true);
  };

  handleClose = (event) => {
    if(event.target.id === 'confirm' || event.target.parentElement.id === 'confirm'){
      const dados = [...this.state.data];
      if(this.state.currentObj.id === null){
        const newObj = {...objetoPadrao};
        newObj.nome = this.state.nome;
				newObj.email = this.state.email;
				newObj.logradouro = this.state.logradouro;
				newObj.cep = this.state.cep;
				newObj.numero = this.state.numero;
				newObj.cidade = this.state.cidade;
				newObj.complemento = this.state.complemento;
        newObj.id = null;

        api.request({
          url,
          method: 'post',
          headers,
          data: newObj
        }).then(response => {
          dados.push(newObj);
          this.setData(dados);
        }).catch(erro => console.log(erro));
      } else {
        const dados = [...this.state.data];
        const index = this.state.data.indexOf(this.state.currentObj);
        dados[index].nome = this.state.nome;
				dados[index].email = this.state.email;
				dados[index].logradouro = this.state.logradouro;
				dados[index].cep = this.state.cep;
				dados[index].numero = this.state.numero;
				dados[index].cidade = this.state.cidade;
				dados[index].complemento = this.state.complemento;

        api.request({
          url: `${url}/${this.state.currentObj.id}`,
          method: 'put',
          headers,
          data: dados[index]
        }).then(response => {
          console.log(response);
          this.setData(dados);
        }).catch(erro => console.log(erro));
      }
    }

    this.setOpen(false);
  };

  handleDelete = (event, rowData) => {
    this.setCurrentObj(rowData);
    this.setOpenConfirm(true);
  }

  handleCloseConfirm = (event) => {
    if(event.target.id === 'confirm' || event.target.parentElement.id === 'confirm'){
      api.request({
        url: `${url}/${this.state.currentObj.id}`,
        method: 'delete',
        headers
      }).then(response => {
        const newData = this.state.data.filter(dado => dado.id !== this.state.currentObj.id);
        this.setData(newData);
      }).catch(erro => console.log(erro));
    }
    this.setOpenConfirm(false);
  }

	render() {
    return (
      <Container fixed>
        <ActionTable
          title={'Clientes'}
          columns={this.state.columns}
          data={this.state.data}
          setData={this.setData}
          onAdd={this.handleClickOpenAdd}
          onEdit={this.handleClickOpen}
          onDelete={this.handleDelete}
        />
        <ClientesForm
          open={this.state.open}
          handleClose={this.handleClose}
          nome={this.state.nome}
          email={this.state.email}
          logradouro={this.state.logradouro}
          cep={this.state.cep}
          numero={this.state.numero}
          cidade={this.state.cidade}
          complemento={this.state.complemento}
          setNome={this.setNome}
          setEmail={this.setEmail}
          setLogradouro={this.setLogradouro}
          setCep={this.setCep}
          setNumero={this.setNumero}
          setCidade={this.setCidade}
          setComplemento={this.setComplemento}
        />
        <ConfirmDialog
          open={this.state.openConfirm}
          handleClose={this.handleCloseConfirm}
          title={"Tem certeza que deseja excluir o registro?"}
        />
      </Container>
    );
	};
}

export default withRouter(Clientes);
