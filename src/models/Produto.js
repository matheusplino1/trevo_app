export default class Produto {

  id = null;
  descricao = null;
  preco = null;
  quantidade = null;
  thumbnail = null;

  static fromDataRequest(data){
    const arrayProdutos = [];
    for(let prod of data){
      arrayProdutos.push(this.fromData(prod));
    }
    return arrayProdutos;
  }

  static fromData(data){
    const produto = new Produto();
    produto.setId(data.id)
           .setDescricao(data.descricao)
           .setPreco(data.preco)
           .setQuantidade(data.quantidade)
           .setThumbnail(data.thumbnail);
           
    return produto;
  }

  getFormData = () => {
    const
      formData = new FormData(),
      thumb = this.getThumbnail();

    formData.append('descricao', this.getDescricao());
    formData.append('preco', this.getPreco());
    formData.append('quantidade', this.getQuantidade());

    if(!!thumb){
      formData.append('thumbnail', this.getThumbnail());
    }

    return formData;
  }

  /**
   * Setter para propriedade id
   * @param id
   * @return instance
   */
  setId = (id) => {
    this.id = id;
    return this;
  };

  /**
   * Getter para propriedade id
   * @return id
   */
  getId = () => {
    return this.id;
  }

  /**
   * Setter para propriedade descricao
   * @param descricao
   * @return instance
   */
  setDescricao = (descricao) => {
    this.descricao = descricao;
    return this;
  };

  /**
   * Getter para propriedade descricao
   * @return descricao
   */
  getDescricao = () => {
    return this.descricao;
  }

  /**
   * Setter para propriedade preco
   * @param preco
   * @return instance
   */
  setPreco = (preco) => {
    this.preco = preco;
    return this;
  };

  /**
   * Getter para propriedade preco
   * @return preco
   */
  getPreco = () => {
    return this.preco;
  }

  /**
   * Setter para propriedade quantidade
   * @param quantidade
   * @return instance
   */
  setQuantidade = (quantidade) => {
    this.quantidade = quantidade;
    return this;
  };

  /**
   * Getter para propriedade quantidade
   * @return quantidade
   */
  getQuantidade = () => {
    return this.quantidade;
  }

  /**
   * Setter para propriedade thumbnail
   * @param thumbnail
   * @return instance
   */
  setThumbnail = (thumbnail) => {
    this.thumbnail = thumbnail;
    return this;
  };

  /**
   * Getter para propriedade thumbnail
   * @return thumbnail
   */
  getThumbnail = () => {
    return this.thumbnail;
  }
}
