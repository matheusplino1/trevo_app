export default class Usuario {

  id = null;
  perfil = null;
  nome = null;
  email = null;
  senha = null;
  logradouro = null;
  cep = null;
  numero = null;
  cidade = null;
  complemento = null;

  static fromDataRequest(data){
    const arrayUsuarios = [];
    for(let usuario of data){
      arrayUsuarios.push(this.fromData(usuario));
    }
    return arrayUsuarios;
  }

  static fromData(data){
    const usuario = new Usuario();
    usuario.setId(data.id)
           .setPerfil(data.perfil)
           .setNome(data.nome)
           .setEmail(data.email)
           .setSenha(data.senha)
           .setLogradouro(data.logradouro)
           .setCep(data.cep)
           .setNumero(data.numero)
           .setCidade(data.cidade)
           .setComplemento(data.complemento);

    return usuario;
  }

  getFormData = () => {
    const
      formData = new FormData(),
      senha = this.getSenha(),
      numero = this.getNumero();

    formData.append('perfil', this.getPerfil());
    formData.append('nome', this.getNome());
    formData.append('email', this.getEmail());
    formData.append('logradouro', this.getLogradouro());
    formData.append('cep', this.getCep());
    formData.append('cidade', this.getCidade());
    formData.append('complemento', this.getComplemento());

    if (!!senha && senha !== '') {
      formData.append('senha', senha);
    }

    if (!!numero && numero !== '') {
      formData.append('numero', numero);
    }

    return formData;
  }

  /**
   * Setter para propriedade id
   * @param id
   * @return instance
   */
  setId = (id) => {
    this.id = id;
    return this;
  };

  /**
   * Getter para propriedade id
   * @return id
   */
  getId = () => {
    return this.id;
  }

  /**
   * Setter para propriedade perfil
   * @param perfil
   * @return instance
   */
  setPerfil = (perfil) => {
    this.perfil = perfil;
    return this;
  };

  /**
   * Getter para propriedade perfil
   * @return perfil
   */
  getPerfil = () => {
    return this.perfil;
  }

  /**
   * Setter para propriedade nome
   * @param nome
   * @return instance
   */
  setNome = (nome) => {
    this.nome = nome;
    return this;
  };

  /**
   * Getter para propriedade nome
   * @return nome
   */
  getNome = () => {
    return this.nome;
  }

  /**
   * Setter para propriedade email
   * @param email
   * @return instance
   */
  setEmail = (email) => {
    this.email = email;
    return this;
  };

  /**
   * Getter para propriedade email
   * @return email
   */
  getEmail = () => {
    return this.email;
  }

  /**
   * Setter para propriedade senha
   * @param senha
   * @return instance
   */
  setSenha = (senha) => {
    this.senha = senha;
    return this;
  };

  /**
   * Getter para propriedade senha
   * @return senha
   */
  getSenha = () => {
    return this.senha;
  }

  /**
   * Setter para propriedade logradouro
   * @param logradouro
   * @return instance
   */
  setLogradouro = (logradouro) => {
    this.logradouro = logradouro;
    return this;
  };

  /**
   * Getter para propriedade logradouro
   * @return senha
   */
  getLogradouro = () => {
    return this.logradouro;
  }

  /**
   * Setter para propriedade cep
   * @param cep
   * @return instance
   */
  setCep = (cep) => {
    this.cep = cep;
    return this;
  };

  /**
   * Getter para propriedade cep
   * @return senha
   */
  getCep = () => {
    return this.cep;
  }

  /**
   * Setter para propriedade numero
   * @param numero
   * @return instance
   */
  setNumero = (numero) => {
    this.numero = numero;
    return this;
  };

  /**
   * Getter para propriedade numero
   * @return senha
   */
  getNumero = () => {
    return this.numero;
  }

  /**
   * Setter para propriedade cidade
   * @param cidade
   * @return instance
   */
  setCidade = (cidade) => {
    this.cidade = cidade;
    return this;
  };

  /**
   * Getter para propriedade cidade
   * @return senha
   */
  getCidade = () => {
    return this.cidade;
  }

  /**
   * Setter para propriedade complemento
   * @param complemento
   * @return instance
   */
  setComplemento = (complemento) => {
    this.complemento = complemento;
    return this;
  };

  /**
   * Getter para propriedade complemento
   * @return senha
   */
  getComplemento = () => {
    return this.complemento;
  }

}
