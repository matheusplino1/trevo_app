import React from 'react';
import 'typeface-roboto';
import { createMuiTheme, ThemeProvider } from '@material-ui/core/styles';
import teal from '@material-ui/core/colors/teal';
import { ptBR } from '@material-ui/core/locale';
// import HttpsRedirect from 'react-https-redirect';

import Routes from './routes';

const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#00e676',
      contrastText: '#fff',
    },
    secondary: teal,
  },
}, ptBR);

function App() {
    return (
        <ThemeProvider theme={theme}>
          <div className="container">
              <div className="content">
                  <Routes />
              </div>
          </div>
        </ThemeProvider>
    );
}

export default App;
