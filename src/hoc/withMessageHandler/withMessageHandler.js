import React, { Component } from 'react';

import SnackBar from '../../components/SnackBar';
import ComponentWrapper from '../ComponentWrapper/ComponentWrapper';

const withMessageHandler = ( WrappedComponent ) => {
    return class extends Component {
        state = {
            showMessage: false,
            message: '',
            variant: 'info'
        }

        messageConfirmedHandler = () => this.setState({showMessage: false});
        setShowMessage = (bShow) => this.setState({showMessage: bShow});
        setMessage = (mensagem) => this.setState({message: mensagem});
        setVariant = (variant) => this.setState({variant: variant});

        handleError = (error) => {
          let mensagem = error.message;

          if (error.message === 'Network Error'){
            mensagem = "Não foi possível conectar com a API.";
          } else if (!!error.response && !!error.response.data && !!error.response.data.message) {
            mensagem = error.response.data.message;
          }

          this.openSnack(mensagem, 'error');
        }

        handleSuccess = (mensagem) => this.openSnack(mensagem, 'success');
        handleInfo = (mensagem) => this.openSnack(mensagem, 'info');
        handleWarning = (mensagem) => this.openSnack(mensagem, 'warning');

        openSnack = (mensagem, variant) => {
          this.setMessage(mensagem);
          this.setVariant(variant);
          this.setShowMessage(true);
        }

        render () {
            return (
                <ComponentWrapper>
                    <SnackBar
                      variant     = {this.state.variant}
                      message     = {this.state.message}
                      handleClose = {this.messageConfirmedHandler}
                      open        = {this.state.showMessage}
                    />
                    <WrappedComponent
                      handleError   = {this.handleError}
                      handleSuccess = {this.handleSuccess}
                      handleInfo    = {this.handleInfo}
                      handleWarning = {this.handleWarning}
                      {...this.props}
                    />
                </ComponentWrapper>
            );
        }
    }
}

export default withMessageHandler;
