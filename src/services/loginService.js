const loginService = {

  apiUrl: 'http://localhost:3003',
  // apiUrl: 'https://localhost:3003',
  login: (token) => localStorage.setItem('token', token),
  logout: () => localStorage.setItem('token', null),
  getToken: () => localStorage.getItem('token'),

  verificaLogin: () => {
    const token = localStorage.getItem('token');
    const menu = localStorage.getItem('menu');

    if(!token || token === "null") {
      return false;
    } else if(!menu || menu === "null"){
      return false;
    }
    return true;
  },


}

export default loginService;
