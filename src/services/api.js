import axios from 'axios';
import loginService from './loginService';

const token = loginService.getToken();

if(!!token && token !== 'null'){
	axios.defaults.headers.common['Authorization'] = `Bearer ${token}`;
}

const api = axios.create({
	baseURL: loginService.apiUrl
});

export default api;
