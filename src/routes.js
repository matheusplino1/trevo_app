import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';

import Login from './pages/Login';
import Logout from './pages/Common/Logout';
// import Dashboard from './pages/Dashboard';
import Clientes from './pages/Clientes';
import Produtos from './pages/Produtos';
import Usuarios from './pages/Usuarios';
import EditUsuario from './pages/Usuario';
import CatalogoProdutos from './pages/CatalogoProdutos';
import Visitas from './pages/Visitas';
import Pedidos from './pages/Pedidos';
import PedidoExterno from './pages/PedidoExterno';

export default function Routes() {
	return (
		<BrowserRouter>
			<Switch>
				<Route path="/" exact component={Login} />
        <Route path="/dashboard" component={CatalogoProdutos} />
				<Route path="/catalogoprodutos" component={CatalogoProdutos} />
				<Route path="/visitas" component={Visitas} />
				<Route path="/pedidos" component={Pedidos} />
				<Route path="/pedido/:id" exact component={PedidoExterno} />
        <Route path="/clientes" component={Clientes} />
        <Route path="/produtos" component={Produtos} />
				<Route path="/usuarios" component={Usuarios} />
				<Route path="/configuracoes" component={EditUsuario} />
        <Route path="/sair" component={Logout} />
			</Switch>
		</BrowserRouter>
	);
}
