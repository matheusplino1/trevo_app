import React from 'react';
import MaterialTable from "material-table";
import materialTableConfig from "../common/materialTableConfig";

export default class ActionTable extends React.PureComponent {
  render() {
    return (
      <>
        <MaterialTable
          icons={materialTableConfig.tableIcons}
          title={this.props.title}
          columns={this.props.columns}
          data={this.props.data}
          localization={materialTableConfig.localizationFields}
          actions={[
            {
              icon: materialTableConfig.tableIcons.Edit,
              tooltip: `Editar ${this.props.title.toLowerCase() || 'Editar Registro'}`,
              onClick: this.props.onEdit
            },
            {
              icon: materialTableConfig.tableIcons.Delete,
              tooltip: `Excluir ${this.props.title.toLowerCase() || 'Excluir Registro'}`,
              onClick: this.props.onDelete
            },
            {
              icon: materialTableConfig.tableIcons.Add,
              tooltip: `Adicionar ${this.props.title.toLowerCase() || 'Novo Registro'}`,
              isFreeAction: true,
              onClick: this.props.onAdd
            }
          ]}
        />
        {this.props.children}
      </>
    )
  }
}
