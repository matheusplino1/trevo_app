import React from 'react';
import MaterialTable from "material-table";
import materialTableConfig from "../common/materialTableConfig";

export default class EditableTable extends React.Component {
  render() {
    return (
      <MaterialTable
        icons={materialTableConfig.tableIcons}
        title={this.props.title}
        columns={this.props.columns}
        data={this.props.data}
        localization={materialTableConfig.localizationFields}
        editable={{
          onRowAdd: newData =>
            new Promise((resolve, reject) => {
              setTimeout(() => {
                {
                  const data = [...this.props.data];
                  data.push(newData);
                  this.props.setData(data);
                  resolve();
                }
                resolve();
              }, 1000)
            }),
          onRowUpdate: (newData, oldData) =>
            new Promise((resolve, reject) => {
              setTimeout(() => {
                {
                  const data = [...this.props.data];
                  const index = data.indexOf(oldData);
                  data[index] = newData;

                  this.props.setData(data);
                  resolve();
                }
                resolve();
              }, 1000)
            }),
          onRowDelete: oldData =>
            new Promise((resolve, reject) => {
              setTimeout(() => {
                {
                  let data = this.props.data;
                  let newData = [...data];
                  const index = data.indexOf(oldData);
                  newData.splice(index, 1);
                  this.props.setData(newData);
                }
                resolve();
              }, 1000)
            }),
        }}
      />
    )
  }
}
