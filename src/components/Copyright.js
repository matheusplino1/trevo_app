import React from 'react';
import Typography from '@material-ui/core/Typography';

export default function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {'Copyright © '}
      {'Trevo Distribuidora de Alimentos LTDA'}
      {' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}
