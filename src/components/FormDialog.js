import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';

export default function FormDialog(props) {
  return (
    <Dialog open={props.open} onEnter={props.onEnter} fullWidth={true} maxWidth="sm" onClose={props.handleclose} aria-labelledby="form-dialog-title">
      <DialogTitle id="form-dialog-title">{props.title}</DialogTitle>
      <DialogContent>
        {props.children}
      </DialogContent>
      <DialogActions>
        <Button onClick={props.handleclose} id="cancel" color="secondary">
          Cancelar
        </Button>
        <Button onClick={props.handleclose} id="confirm" color="secondary">
          Salvar
        </Button>
      </DialogActions>
    </Dialog>
  );
}
